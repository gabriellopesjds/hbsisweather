package br.com.gabriellopesjds.hbsisweather.mvp.base;

import br.com.gabriellopesjds.hbsisweather.mvp.data.AppDataManager;

public abstract class BaseModel<T> {
    protected T presenter;
    protected final AppDataManager dataManager = AppDataManager.getInstance();

    public BaseModel(T presenter) {
        this.presenter = presenter;
    }
}
