package br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherlist;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.ListWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public class CadastroWeatherListPresenter implements ListWeatherMVP.Presenter {
    private ListWeatherMVP.View view;
    private CadastroWeatherListModel model;
    private Weather weather;
    private List<Weather> listaWeather, listaWeatherAux;

    public CadastroWeatherListPresenter(ListWeatherMVP.View view) {
        this.view = view;
        this.model = new CadastroWeatherListModel(this);
        this.weather = new Weather();
    }

    @Override
    public Context getContext() {
        return (Context) view;
    }

    @Override
    public void responseCallbackCidade(ResponseCallbackCidade response) {
        view.responseCallbackCidade(response);
    }

    @Override
    public void responseCallbackGravarCliente(ResponseCallbackGravarCliente response) {
        view.responseCallbackGravarCliente(response);
    }

    @Override
    public List<Weather> getWeatherList() {
        return this.listaWeather;
    }

    @Override
    public List<Weather> getWeatherListAux() {
        return this.listaWeatherAux;
    }

    @Override
    public List<Weather> setWeatherList(List<Weather> list) {
        return this.listaWeather = list;
    }

    @Override
    public List<Weather> setWeatherListAux(List<Weather> list) {
        return this.listaWeatherAux = list;
    }

    @Override
    public void callServiceGravarCadastro(Weather weather, String tipoIntent) {
        model.callServiceGravarCadastro(weather, getContext(), tipoIntent);
    }

    @Override
    public void getNetworkdataCidade(String cidade) {
        model.getNetworkdataCidade(getContext(), cidade);
    }

    @Override
    public void unregisterModel() {
        EventBus.getDefault ().unregister ( model );
    }
}