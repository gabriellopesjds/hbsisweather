package br.com.gabriellopesjds.hbsisweather.mvp.data;

import br.com.gabriellopesjds.hbsisweather.mvp.weather.DetailWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.ListWeatherMVP;

// ESTA CLASSE EXTENDE TODAS AS INTERFACES DE PERSISTENCIA LOCAL
// DAS INTERFACES VIEW MODEL EVITANDO A IMPLEMENTAÇÃO MANUAL.
public interface DbHelper extends DetailWeatherMVP.Model.Db, ListWeatherMVP.Model.Db{
     void notificarConclusaoTarefa(Object object);
}
