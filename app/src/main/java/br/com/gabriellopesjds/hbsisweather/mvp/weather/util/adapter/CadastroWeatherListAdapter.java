package br.com.gabriellopesjds.hbsisweather.mvp.weather.util.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import br.com.gabriellopesjds.hbsisweather.R;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.util.RecyclerViewOnClickListenerHack;

public class CadastroWeatherListAdapter extends RecyclerView.Adapter {
    private List<Weather> listaWeather;
    private Context context;
    private RecyclerViewOnClickListenerHack recyclerViewOnClickListenerHack;

    public CadastroWeatherListAdapter(List<Weather> listaWeather, Context context) {
        this.listaWeather = listaWeather;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cadastro_weather, parent, false);
        CadastroWeatherViewHolder cadastroWeatherViewHolder = new CadastroWeatherViewHolder(view);
        return cadastroWeatherViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        CadastroWeatherViewHolder cadastroWeatherViewHolder = (CadastroWeatherViewHolder) holder;
        Weather weather = listaWeather.get(position);
        cadastroWeatherViewHolder.getNomeCidade().setText(" " + weather.getNomeCidade() + " / " + weather.getNomePais());
        cadastroWeatherViewHolder.getTempetatura().setText(" " + weather.getTemperaturaAtual() +  " ℃");
        int drawableId = context.getResources().getIdentifier(weather.getImagem(), "drawable", context.getPackageName());
        cadastroWeatherViewHolder.getIcone().setImageResource(drawableId);
        cadastroWeatherViewHolder.getDataAtualizacao().setText(" " + weather.getDataAtualizacao());

    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r) {
        this.recyclerViewOnClickListenerHack = r;
    }

    public void adicionarElementoNaLista(Weather weather, int position) {
        listaWeather.add(position, weather);
        notifyItemInserted(position);
    }

    public void removerElementoDaLista(int position) {
        listaWeather.remove(position);
        notifyItemRemoved(position);
    }

    public void atualizarLista(List<Weather> lista){
        listaWeather.clear();
        listaWeather.addAll(lista);
        notifyDataSetChanged();
    }

    public Weather getElemento(int position) {
        return listaWeather.get(position);
    }

    public void setFilter(List<Weather> listaFornecedor) {
        this.listaWeather = new ArrayList<>();
        this.listaWeather.addAll(listaFornecedor);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.listaWeather.size();
    }

    public class CadastroWeatherViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView nomeCidade;
        private ImageView icone;
        private TextView tempetatura;
        private TextView dataAtualizacao;
        private CardView cardView;

        public CadastroWeatherViewHolder(View view) {
            super(view);

            this.nomeCidade = (TextView) view.findViewById(R.id.tv_nomeCidade);
            this.tempetatura = (TextView) view.findViewById(R.id.tv_temperatura);
            this.dataAtualizacao = (TextView) view.findViewById(R.id.tv_dataAtualizacao);
            this.icone = (ImageView) view.findViewById(R.id.iv_icone);
            this.cardView = (CardView) view.findViewById(R.id.cv);

            view.setOnClickListener(this);
            nomeCidade.setOnClickListener(this);
            tempetatura.setOnClickListener(this);
            dataAtualizacao.setOnClickListener(this);
            icone.setOnClickListener(this);
            cardView.setOnClickListener(this);

            view.setOnLongClickListener(this);
            nomeCidade.setOnLongClickListener(this);
            tempetatura.setOnLongClickListener(this);
            dataAtualizacao.setOnLongClickListener(this);
            icone.setOnLongClickListener(this);
            cardView.setOnLongClickListener(this);
        }

        public TextView getNomeCidade() {
            return nomeCidade;
        }

        public TextView getTempetatura() {
            return tempetatura;
        }

        public TextView getDataAtualizacao() {
            return dataAtualizacao;
        }

        public ImageView getIcone() {
            return icone;
        }

        @Override
        public void onClick(View view) {
            if (recyclerViewOnClickListenerHack != null) {
                recyclerViewOnClickListenerHack.onClickListener(view, getPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (recyclerViewOnClickListenerHack != null) {
                recyclerViewOnClickListenerHack.onLongClickListener(view, getPosition());
            }
            return false;
        }
    }
}
