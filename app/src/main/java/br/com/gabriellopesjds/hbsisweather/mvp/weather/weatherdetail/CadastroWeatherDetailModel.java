package br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherdetail;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.gabriellopesjds.hbsisweather.mvp.base.BaseModel;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.DetailWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public class CadastroWeatherDetailModel extends BaseModel implements DetailWeatherMVP.Model, DetailWeatherMVP.Model.Db, DetailWeatherMVP.Model.Network, DetailWeatherMVP.Model.Preferences {
    private DetailWeatherMVP.Presenter presenter;


    public CadastroWeatherDetailModel(DetailWeatherMVP.Presenter presenter) {
        super(presenter);
        this.presenter = presenter;
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }
    @Override
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object event) {
        if (event instanceof ResponseCallbackConsultaCidade)
            responseCallbackConsultaCidade((ResponseCallbackConsultaCidade) event);
    }

    @Override
    public void getNetworkdataCidade(Context context, Weather weather) {
        dataManager.getNetworkdataCidade(context, weather);
    }

    @Override
    public void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response) {
        presenter.responseCallbackConsultaCidade(response);
    }
}