package br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherlist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.gabriellopesjds.hbsisweather.mvp.base.BaseModel;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.ListWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public class CadastroWeatherListModel extends BaseModel implements ListWeatherMVP.Model, ListWeatherMVP.Model.Db, ListWeatherMVP.Model.Network, ListWeatherMVP.Model.Preferences {
    private ListWeatherMVP.Presenter presenter;


    public CadastroWeatherListModel(ListWeatherMVP.Presenter presenter) {
        super(presenter);
        this.presenter = presenter;
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void callServiceGravarCadastro(Weather weather, Context context, String tipoIntent) {
        dataManager.callServiceGravarCadastro(weather, context, tipoIntent);
    }

    @Override
    public void responseCallbackGravarCliente(ResponseCallbackGravarCliente response) {
        presenter.responseCallbackGravarCliente(response);
    }

    @Override
    public SQLiteDatabase getDatabase() {
        return dataManager.getDatabase();
    }


    @Override
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object event) {
        if (event instanceof ResponseCallbackCidade)
            responseCallbackCidade((ResponseCallbackCidade) event);
        if(event instanceof ResponseCallbackGravarCliente)
            responseCallbackGravarCliente((ResponseCallbackGravarCliente) event);
    }

    @Override
    public void getNetworkdataCidade(Context context, String cidade) {
        dataManager.getNetworkdataCidade(context, cidade);
    }

    @Override
    public void responseCallbackCidade(ResponseCallbackCidade response) {
        presenter.responseCallbackCidade(response);
    }
}
