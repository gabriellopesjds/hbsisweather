package br.com.gabriellopesjds.hbsisweather.mvp.weather.util.observer;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;

public interface ComunicadorWeather {
    void updateWeather(Weather weather);
}
