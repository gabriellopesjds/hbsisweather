package br.com.gabriellopesjds.hbsisweather.mvp.weather.data;

import android.content.Context;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkCidade {

    private static final String OPEN_WEATHER_MAP_API =
            "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric";
    private static final String BASE = "http://api.openweathermap.org/data/2.5/forecast?cnt=5&q=%s&units=metric";

    public static JSONObject getJSON(Context context, String city) {
        try {
//            URL url2 = new URL(String.format(BASE, city));
//            HttpURLConnection connection2 =
//                    (HttpURLConnection) url2.openConnection();


//            connection2.addRequestProperty("x-api-key",
//                    "1843a3a0ee5f3b42f5fc201c4c81fe2a");

            URL url = new URL(String.format(OPEN_WEATHER_MAP_API, city));
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();
            connection.addRequestProperty("x-api-key",
                    "1843a3a0ee5f3b42f5fc201c4c81fe2a");

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp = "";
            while ((tmp = reader.readLine()) != null)
                json.append(tmp).append("\n");
            reader.close();

            JSONObject data = new JSONObject(json.toString());

            if (data.getInt("cod") != 200) {
                return null;
            }

            return data;
        } catch (Exception e) {
            return null;
        }
    }
}
