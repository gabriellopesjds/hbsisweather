package br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherlist;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import br.com.gabriellopesjds.hbsisweather.R;
import br.com.gabriellopesjds.hbsisweather.mvp.base.BaseHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.AppDataManager;
import br.com.gabriellopesjds.hbsisweather.mvp.data.db.dao.WeatherDAO;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.ListWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.adapter.CadastroWeatherListAdapter;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.EventIntent;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherdetail.CadastroWeatherDetailActivity;
import br.com.gabriellopesjds.hbsisweather.mvp.util.RecyclerViewOnClickListenerHack;

public class CadastroWeatherListActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack, SearchView.OnQueryTextListener, ListWeatherMVP.View {
    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView;
    private AlertDialog alertDialog;
    private CadastroWeatherListAdapter adapter;
    private int IDENTIFICADOR_WEATHER_STEP = 0;
    private ListWeatherMVP.Presenter presenter;
    private int position;
    private int check = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cadastro);
        BaseHelper.getInstance(this);
        AppDataManager.getInstance();

        if (presenter == null)
            presenter = new CadastroWeatherListPresenter(this);

        configurarToolbar();
        initView();
        setListener();
        configurarRecyclerView();

    }

    private void configurarRecyclerView() {
        recyclerView.setHasFixedSize(true);
        presenter.setWeatherList(new WeatherDAO(this).getAll());
        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layout);
        adapter = new CadastroWeatherListAdapter(presenter.getWeatherList(), this);
        adapter.setRecyclerViewOnClickListenerHack(this);
        recyclerView.setAdapter(adapter);
    }

    private void configurarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Cidades");
    }


    public void initView() {
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    }

    public void setListener() {
        floatingActionButton.setOnClickListener(view -> chamarDialogNovoCadastro());
    }

    private void chamarDialogNovoCadastro() {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.item_dialog_nova_cidade, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText etCartao = (EditText) view.findViewById(R.id.et_nome_cidade);

        etCartao.setOnFocusChangeListener((v, hasFocus) -> alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE));

        etCartao.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                alertDialog.dismiss();
                presenter.getNetworkdataCidade(etCartao.getText().toString());
            }
            return false;
        });

        builder.setPositiveButton("Confirmar", (dialog, which) -> {
            alertDialog.dismiss();
            presenter.getNetworkdataCidade(etCartao.getText().toString());
        });

        builder.setNegativeButton("Cancelar", (dialog, which) -> alertDialog.dismiss());

        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onClickListener(View view, int position) {
        Intent intent = new Intent(getBaseContext(), CadastroWeatherDetailActivity.class);
        EventIntent event = new EventIntent(adapter.getElemento(position), "UPDATE", position);
        EventBus.getDefault().postSticky(event);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, IDENTIFICADOR_WEATHER_STEP);
    }

    @Override
    public void onLongClickListener(View view, int position) {
        CadastroWeatherListAdapter adapter = (CadastroWeatherListAdapter) recyclerView.getAdapter();
        chamarDialogRemover(adapter.getElemento(position), position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ordenacao, menu);
        getMenuInflater().inflate(R.menu.menu_searchview, menu);
        MenuItem searchItem = menu.findItem(R.id.action_searchable_activity);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.item_ordenar) {
            chamarDialogOrdenacao();
        }
        return super.onOptionsItemSelected(item);
    }

    private void chamarDialogOrdenacao() {
        final ArrayList<CharSequence> listaDeSequences = new ArrayList<>();
        listaDeSequences.add("Nome");
        listaDeSequences.add("Menor Tempetaratura");
        listaDeSequences.add("Maior Tempetaratura");

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Ordenar:");
        mBuilder.setSingleChoiceItems(listaDeSequences.toArray(new CharSequence[listaDeSequences.size()]), check, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        Collections.sort(presenter.getWeatherList(), (o1, o2) -> {
                            return o1.getTemperaturaAtual().compareTo(o2.getNomeCidade());
                        });
                        adapter.notifyDataSetChanged();
                        alertDialog.dismiss();
                        check = 0;
                        break;
                    case 1:
                        Collections.sort(presenter.getWeatherList(), (o1, o2) -> {
                            if (Double.parseDouble(o1.getTemperaturaAtual().replaceAll(",", ".")) < Double.parseDouble(o2.getTemperaturaAtual().replaceAll(",", ".")))
                                return -1;
                            else if (Double.parseDouble(o1.getTemperaturaAtual().replaceAll(",", ".")) > Double.parseDouble(o2.getTemperaturaAtual().replaceAll(",", ".")))
                                return 1;
                            else return 0;
                        });
                        adapter.notifyDataSetChanged();
                        alertDialog.dismiss();
                        check = 1;
                        break;
                    case 2:
                        Collections.sort(presenter.getWeatherList(), (o1, o2) -> {
                            if (Double.parseDouble(o2.getTemperaturaAtual().replaceAll(",", ".")) < Double.parseDouble(o1.getTemperaturaAtual().replaceAll(",", ".")))
                                return -1;
                            else if (Double.parseDouble(o2.getTemperaturaAtual().replaceAll(",", ".")) > Double.parseDouble(o1.getTemperaturaAtual().replaceAll(",", ".")))
                                return 1;
                            else return 0;
                        });
                        adapter.notifyDataSetChanged();
                        alertDialog.dismiss();
                        check = 2;
                        break;
                }
            }
        });

        alertDialog = mBuilder.create();
        alertDialog.show();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        CadastroWeatherListAdapter adapter = (CadastroWeatherListAdapter) recyclerView.getAdapter();
        if (newText.equals("")) {
            adapter.setFilter(presenter.getWeatherList());
        } else {
            adapter.setFilter(filter(presenter.getWeatherList(), newText));
        }
        return true;
    }

    private List<Weather> filter(List<Weather> lista, String query) {
        query = query.toLowerCase();
        String finalQuery = query;
        presenter.setWeatherListAux(new ArrayList<>());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            presenter.setWeatherListAux(lista.stream().filter(w -> w.getNomeCidade().toLowerCase().contains(finalQuery)).collect(Collectors.toList()));
        } else {
            for (Weather w : lista) {
                final String text = w.getNomeCidade().toLowerCase();
                if (text.contains(query)) {
                    presenter.getWeatherListAux().add(w);
                }
            }
        }
        return presenter.getWeatherListAux();
    }

    private void chamarDialogRemover(final Weather weather, final int position) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Atenção, deseja excluir a cidade ?");

        mBuilder.setMessage("Cidade" + " '" + weather.getNomeCidade() + "' ?");
        mBuilder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CadastroWeatherListAdapter adapter = (CadastroWeatherListAdapter) recyclerView.getAdapter();
                adapter.removerElementoDaLista(position);
                if (presenter.getWeatherList().contains(weather)) {
                    presenter.getWeatherList().remove(weather);
                }
                new WeatherDAO(getBaseContext()).deleteWeather(weather);
                alertDialog.dismiss();
                Toast.makeText(getWindow().getContext(), "Cidade '" + weather.getNomeCidade() + "' excluido com sucesso !", Toast.LENGTH_LONG).show();
            }
        });
        mBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = mBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IDENTIFICADOR_WEATHER_STEP) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Weather weather = (Weather) extras.getSerializable(Weather.KEY_WEATHER);
                position = extras.getInt("position", 0);
                presenter.callServiceGravarCadastro(weather, extras.getString("tipoIntent"));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        EventBus.getDefault().postSticky(presenter);
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unregisterModel();
        Weather stickyEvent = EventBus.getDefault().removeStickyEvent(Weather.class);
        if (stickyEvent != null) {
            EventBus.getDefault().removeStickyEvent(Weather.class);
        }
    }

    @Override
    public void responseCallbackCidade(ResponseCallbackCidade response) {
        if (response.getWeather().getNomeCidade() != null) {
            presenter.callServiceGravarCadastro(response.getWeather(), "CREATE");
        } else
            Toast.makeText(this, "Cidade nao localizada !", Toast.LENGTH_LONG).show();
    }

    @Override
    public void responseCallbackGravarCliente(ResponseCallbackGravarCliente response) {
        if (response.getTipoIntent().equals("UPDATE")) {
            adapter.removerElementoDaLista(position);
            adapter.adicionarElementoNaLista(response.getWeather(), position);
        } else {
            adapter.adicionarElementoNaLista(response.getWeather(), presenter.getWeatherList().size());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}