package br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;

/**
 * Created by gabri on 08/04/2018.
 */

public class ResponseCallbackConsultaCidade {
    private Weather weather;

    public ResponseCallbackConsultaCidade(Weather weather) {
        this.weather = weather;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }
}
