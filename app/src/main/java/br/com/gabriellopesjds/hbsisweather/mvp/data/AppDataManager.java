package br.com.gabriellopesjds.hbsisweather.mvp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.greenrobot.eventbus.EventBus;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public class AppDataManager implements DataManager {
    private final static AppDataManager instance = new AppDataManager();
    private static AppNetworkHelper appNetworkHelper;
    private static AppDbHelper appDbHelper;
    private static AppPreferencesHelper appPreferencesHelper;

    public static AppDataManager getInstance() {
        if (appNetworkHelper == null)
            appNetworkHelper = new AppNetworkHelper(instance);

        if (appDbHelper == null)
            appDbHelper = new AppDbHelper(instance);

        if (appPreferencesHelper == null)
            appPreferencesHelper = new AppPreferencesHelper(instance);

        return instance;
    }

    @Override
    public void callServiceGravarCadastro(Weather weather, Context context, String tipoIntent) {
        appDbHelper.callServiceGravarCadastro(weather, context, tipoIntent);
    }

    @Override
    public void responseCallbackGravarCliente(ResponseCallbackGravarCliente response) {
        EventBus.getDefault().postSticky(response);
    }

    @Override
    public SQLiteDatabase getDatabase() {
        return appDbHelper.getDatabase();
    }

    @Override
    public void getNetworkdataCidade(Context context, String cidade) {
        appNetworkHelper.getNetworkdataCidade(context, cidade);
    }

    @Override
    public void getNetworkdataCidade(Context context, Weather weather) {
        appNetworkHelper.getNetworkdataCidade(context, weather);
    }

    @Override
    public void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response) {
        EventBus.getDefault().postSticky(response);
    }

    @Override
    public void responseCallbackCidade(ResponseCallbackCidade response) {
        EventBus.getDefault().postSticky(response);
    }

    @Override
    public void notificarConclusaoTarefa(Object object) {
    }
}
