package br.com.gabriellopesjds.hbsisweather.mvp.data.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Weather implements Serializable {
    public static final String KEY_WEATHER = "weather";
    private String id;
    private String nomeCidade;
    private String nomePais;
    private String imagem;
    private String temperaturaAtual;
    private String humidadeAtual;
    private String pressaoAtual;
    private String dataAtualizacao;
    private String descricaoAtual;
    private String background;

    public Weather() {
        this.id = UUID.randomUUID().toString();
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public String getNomePais() {
        return nomePais;
    }

    public void setNomePais(String nomePais) {
        this.nomePais = nomePais;
    }

    public String getTemperaturaAtual() {
        return temperaturaAtual;
    }

    public void setTemperaturaAtual(String temperaturaAtual) {
        this.temperaturaAtual = temperaturaAtual;
    }

    public String getHumidadeAtual() {
        return humidadeAtual;
    }

    public void setHumidadeAtual(String humidadeAtual) {
        this.humidadeAtual = humidadeAtual;
    }

    public String getPressaoAtual() {
        return pressaoAtual;
    }

    public void setPressaoAtual(String pressaoAtual) {
        this.pressaoAtual = pressaoAtual;
    }

    public String getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(String dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescricaoAtual(String s) {
        this.descricaoAtual = s;
    }

    public String getDescricaoAtual() {
        return descricaoAtual;
    }

    public void setImagem(String resource) {
        this.imagem = resource;
    }

    public void setImagem(int actualId, long sunrise, long sunset) {
        long tempoAtual = new Date().getTime();
        int id = actualId / 100;
        switch (id) {
            case 2:
                if (tempoAtual >= sunrise && tempoAtual < sunset)
                    imagem = "ic_02d_big";
                else
                    imagem = "ic_02n_big";
                break;
            case 3:
                if (tempoAtual >= sunrise && tempoAtual < sunset)
                    imagem = "ic_03d_big";
                else
                    imagem = "ic_03n_big";
                break;
            case 5:
                if (tempoAtual >= sunrise && tempoAtual < sunset)
                    imagem = "ic_05d_big";
                else
                    imagem = "ic_05n_big";
                break;
            case 6:
                if (tempoAtual >= sunrise && tempoAtual < sunset)
                    imagem = "ic_06d_big";
                else
                    imagem = "ic_06n_big";
                break;
            case 7:
                if (tempoAtual >= sunrise && tempoAtual < sunset)
                    imagem = "ic_07d_big";
                else
                    imagem = "ic_07n_big";
                break;
            case 8:
                if (tempoAtual >= sunrise && tempoAtual < sunset)
                    imagem = "ic_08d_big";
                else
                    imagem = "ic_08n_big";
                break;
        }
    }

    public String getBackground() {
        switch (imagem) {
            case "ic_02d_big":
                return "img_02d_background";
            case "ic_02n_big":
                return "img_02n_background";
            case "ic_03d_big":
                return "img_03d_background";
            case "ic_03n_big":
                return "img_03n_background";
            case "ic_05d_big":
                return "img_05d_background";
            case "ic_05n_big":
                return "img_05n_background";
            case "ic_06d_big":
                return "img_06d_background";
            case "ic_06n_big":
                return "img_06n_background";
            case "ic_07d_big":
                return "img_07d_background";
            case "ic_07n_big":
                return "img_07n_background";
            case "ic_08d_big":
                return "img_08d_background";
            case "ic_08n_big":
                return "img_08n_background";
        }
        return "img_08d_background";
    }

    public String getImagem() {
        return this.imagem;
    }
}