package br.com.gabriellopesjds.hbsisweather.mvp.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import br.com.gabriellopesjds.hbsisweather.mvp.data.AppDataManager;
import br.com.gabriellopesjds.hbsisweather.mvp.data.db.DbCore;

public abstract class BaseHelper {
    protected AppDataManager dataManager;
    protected static SQLiteDatabase db;

    public BaseHelper(AppDataManager appDataManager) {
        if (dataManager == null)
            dataManager = appDataManager;
    }

    public static void getInstance(Context ctx) {
        if (db == null || !db.isOpen()) {
            DbCore helper = new DbCore(ctx);
            db = helper.getWritableDatabase();
        }
    }

    public SQLiteDatabase getDatabase() {
        return this.db;
    }
}
