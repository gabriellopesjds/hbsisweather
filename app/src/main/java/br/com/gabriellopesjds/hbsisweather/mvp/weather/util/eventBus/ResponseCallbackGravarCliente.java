package br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;

/**
 * Created by gabri on 08/04/2018.
 */

public class ResponseCallbackGravarCliente {
    private Weather weather;
    private String tipoIntent;


    public ResponseCallbackGravarCliente(Weather weather, String tipoIntent) {
        this.weather = weather;
        this.tipoIntent = tipoIntent;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public String getTipoIntent() {
        return tipoIntent;
    }

    public void setTipoIntent(String tipoIntent) {
        this.tipoIntent = tipoIntent;
    }
}
