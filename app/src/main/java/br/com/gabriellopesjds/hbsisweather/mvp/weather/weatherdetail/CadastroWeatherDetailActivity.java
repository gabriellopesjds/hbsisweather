package br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherdetail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.gabriellopesjds.hbsisweather.R;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.util.Conexao;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.DetailWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.EventIntent;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;

public class CadastroWeatherDetailActivity extends AppCompatActivity implements DetailWeatherMVP.View {
    private AlertDialog alertDialog;
    private DetailWeatherMVP.Presenter presenter;
    private FragmentManager fm = getSupportFragmentManager();
    CadastroWeatherDetailFragment fornecedorFragment;
    private CoordinatorLayout background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_weather);
        background = (CoordinatorLayout) findViewById(R.id.main_content);
        configurarToolbar();

        if (presenter == null)
            presenter = new CadastroWeatherDetailPresenter(this);

        EventBus.getDefault().register(this);

        if (savedInstanceState == null) {
            fornecedorFragment = new CadastroWeatherDetailFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.content, fornecedorFragment, "fornecedorFragment");
            ft.commit();
        }
    }

    private void configurarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Previsão do Tempo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        Bundle extras = new Bundle();
        extras.putSerializable(Weather.KEY_WEATHER, presenter.getWeather());
        extras.putInt("position", presenter.getPositionItemUpdate());
        extras.putString("tipoIntent", presenter.getTipoIntent());
        intent.putExtras(extras);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onStart() {
        EventBus.getDefault().postSticky(presenter);
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unregisterModel();
        EventIntent stickyEvent = EventBus.getDefault().removeStickyEvent(EventIntent.class);
        if (stickyEvent != null) {
            EventBus.getDefault().removeStickyEvent(EventIntent.class);
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object object) {
        if (object instanceof EventIntent) {
            presenter.setWeather(((EventIntent) object).getWeather());
            int drawableIdBackground = getResources().getIdentifier(presenter.getWeather().getBackground(), "drawable", getPackageName());
            background.setBackgroundResource(drawableIdBackground);

            if (Conexao.verificarConexaoRede(this)) {
                presenter.getNetworkdataConsultaCidade(((EventIntent) object).getWeather());
            } else {
                Snackbar.make(getWindow().getDecorView(), "Sem conexão com Internet, verifique sua conexão para atualizar!", Snackbar.LENGTH_INDEFINITE).show();
            }
            presenter.verificarTipoIntent((EventIntent) object);
        }
    }

    @Override
    public void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response) {
        EventBus.getDefault().postSticky(response.getWeather());
    }

    public Weather getWeather() {
        return presenter.getWeather();
    }
}