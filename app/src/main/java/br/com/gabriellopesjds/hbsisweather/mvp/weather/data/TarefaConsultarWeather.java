package br.com.gabriellopesjds.hbsisweather.mvp.weather.data;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.gabriellopesjds.hbsisweather.mvp.data.AppNetworkHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.NetworkHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;

public class TarefaConsultarWeather extends AsyncTask<String, Void, Void> {
    private Context context;
    private Weather weather;
    private ProgressDialog progress;
    private NetworkHelper callback;

    public TarefaConsultarWeather(Context context, NetworkHelper callback) {
        this.weather = new Weather();
        this.context = context;
        this.callback = callback;
    }

    public TarefaConsultarWeather(Context context, AppNetworkHelper callback, Weather weather) {
        this.weather = weather;
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        progress = new ProgressDialog(context);
        progress.setMessage("Consultando cidade  ...");
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progress.dismiss();
        if (context.getClass().getSimpleName().equals("CadastroWeatherDetailActivity"))
            callback.notificarConclusaoTarefa(new ResponseCallbackConsultaCidade(weather));
        if (context.getClass().getSimpleName().equals("CadastroWeatherListActivity"))
            callback.notificarConclusaoTarefa(new ResponseCallbackCidade(weather));
        super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(String... params) {
        final JSONObject json = NetworkCidade.getJSON(context, params[0]);
        if (json != null)
            renderWeather(json);
        return null;
    }

    private void renderWeather(JSONObject json) {
        try {
// NESTA PARTE EU IRIA FORMAR OS DIAS, PORÉM OBSERVEI QUE A API RETORNA DE HORAS EM HORAS, PARA UTILIZAR O BREAKPOINT DA API DE DIAS É NECESSARIO COMPRAR A LICENÇA

//            JSONObject city = json.getJSONObject("city");
//            JSONArray list = json.getJSONArray("list");
//            String nomeCidade = city.getString("name").toUpperCase(Locale.US);
//            String nomePais = city.getString("country");
//
//            for (int x = 0; x < list.length(); x++) {
//                JSONObject obj = list.getJSONObject(x);
//                JSONObject weather = obj.getJSONArray("weather").getJSONObject(0);
//                JSONObject main = obj.getJSONObject("main");
//                String data = df.format(new Date(obj.getLong("dt") * 1000));
//                String descricao = weather.getString("description").toUpperCase(Locale.US);
//                String imagem = weather.getString("icon");
//                String tempetatura = String.format("%.2f", main.getDouble("temp"));
//                String humidade = main.getString("humidity") + "%";
//                String pressao = main.getString(("pressure")) + " hPa";
//            }

            DateFormat df = DateFormat.getDateTimeInstance();
            JSONObject main = json.getJSONObject("main");
            JSONObject details = json.getJSONArray("weather").getJSONObject(0);
            weather.setNomeCidade(json.getString("name").toUpperCase(Locale.US));
            weather.setNomePais(json.getJSONObject("sys").getString("country"));
            weather.setTemperaturaAtual(String.format("%.2f", main.getDouble("temp")));
            weather.setDescricaoAtual(details.getString("description").toUpperCase(Locale.US));
            weather.setHumidadeAtual(main.getString("humidity") + "%");
            weather.setPressaoAtual(main.getString("pressure") + " hPa");
            weather.setDataAtualizacao(df.format(new Date(json.getLong("dt") * 1000)));
            weather.setImagem(details.getInt("id"),
                    json.getJSONObject("sys").getLong("sunrise") * 1000,
                    json.getJSONObject("sys").getLong("sunset") * 1000);

        } catch (Exception e) {
        }
    }

}
