package br.com.gabriellopesjds.hbsisweather.mvp.weather;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TextInputLayout;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.EventIntent;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;

public interface DetailWeatherMVP {
    interface Presenter {
        Weather getWeather();

        void setWeather(Weather weather);

        Context getContext();

        void verificarTipoIntent(EventIntent eventIntent);

        String getTipoIntent();

        int getPositionItemUpdate();

        void getNetworkdataConsultaCidade(Weather weather);

        void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response);

        void unregisterModel();
    }

    interface View {
        void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response);
    }

    interface Model {
        void onMessageEvent(Object event);

        // responsável por métodos de SQlite
        interface Db {
        }

        //Responsável por métodos de SharedPreferences
        interface Preferences {
        }

        // Responsavel por métodos de rede.
        interface Network {
            void getNetworkdataCidade(Context context, Weather weather);

            void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response);
        }
    }
}