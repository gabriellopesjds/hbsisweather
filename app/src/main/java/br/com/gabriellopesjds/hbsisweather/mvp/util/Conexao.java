package br.com.gabriellopesjds.hbsisweather.mvp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Windows on 04/01/2017.
 */

public abstract class Conexao {
    public static boolean verificarConexaoRede(Context context) {
        ConnectivityManager mConnectivityManager;
        mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

}
