package br.com.gabriellopesjds.hbsisweather.mvp.data.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import br.com.gabriellopesjds.hbsisweather.mvp.base.BaseHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.AppDataManager;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.util.ConstantTables;


public class WeatherDAO implements ConstantTables {
    private Context context;
    private final AppDataManager instance = new AppDataManager();

    public WeatherDAO(Context context) {
        BaseHelper.getInstance(context);
        AppDataManager.getInstance();

        this.context = context;
    }

    public void addWeather(Weather weather) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_WEATHER_ID, weather.getId());
        values.put(COLUMN_WEATHER_NOME_CIDADE, weather.getNomeCidade());
        values.put(COLUMN_WEATHER_NOME_PAIS, weather.getNomePais());
        values.put(COLUMN_WEATHER_IMAGEM, weather.getImagem());
        values.put(COLUMN_WEATHER_TEMPERATURA_ATUAL, weather.getTemperaturaAtual());
        values.put(COLUMN_WEATHER_HUMIDADE_ATUAL, weather.getHumidadeAtual());
        values.put(COLUMN_WEATHER_PRESSAO_ATUAL, weather.getPressaoAtual());
        values.put(COLUMN_WEATHER_DATA_ATUALIZAZAO, String.valueOf(weather.getDataAtualizacao()));

        instance.getDatabase().insert(TABLE_WEATHER, null, values);
    }

    public List<Weather> getAll() {
        String[] columns = {
                COLUMN_WEATHER_ID,
                COLUMN_WEATHER_NOME_CIDADE,
                COLUMN_WEATHER_NOME_PAIS,
                COLUMN_WEATHER_IMAGEM,
                COLUMN_WEATHER_TEMPERATURA_ATUAL,
                COLUMN_WEATHER_HUMIDADE_ATUAL,
                COLUMN_WEATHER_PRESSAO_ATUAL,
                COLUMN_WEATHER_DATA_ATUALIZAZAO
        };

        String sortOrder = COLUMN_WEATHER_NOME_CIDADE + " ASC";
        List<Weather> listaWeather = new ArrayList<>();

        Cursor cursor = instance.getDatabase().query(TABLE_WEATHER,
                columns,
                null,
                null,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                Weather weather = new Weather();
                weather.setId(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_ID)));
                weather.setNomeCidade(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_NOME_CIDADE)));
                weather.setNomePais(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_NOME_PAIS)));
                weather.setImagem(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_IMAGEM)));
                weather.setTemperaturaAtual(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_TEMPERATURA_ATUAL)));
                weather.setHumidadeAtual(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_HUMIDADE_ATUAL)));
                weather.setPressaoAtual(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_PRESSAO_ATUAL)));
                weather.setDataAtualizacao(cursor.getString(cursor.getColumnIndex(COLUMN_WEATHER_DATA_ATUALIZAZAO)));
                listaWeather.add(weather);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return listaWeather;
    }

    public void updateWeather(Weather weather) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_WEATHER_NOME_CIDADE, weather.getNomeCidade());
        values.put(COLUMN_WEATHER_NOME_PAIS, weather.getNomePais());
        values.put(COLUMN_WEATHER_IMAGEM, weather.getImagem());
        values.put(COLUMN_WEATHER_TEMPERATURA_ATUAL, weather.getTemperaturaAtual());
        values.put(COLUMN_WEATHER_HUMIDADE_ATUAL, weather.getHumidadeAtual());
        values.put(COLUMN_WEATHER_PRESSAO_ATUAL, weather.getPressaoAtual());
        values.put(COLUMN_WEATHER_DATA_ATUALIZAZAO, String.valueOf(weather.getDataAtualizacao()));

        instance.getDatabase().update(TABLE_WEATHER, values, COLUMN_WEATHER_ID + " = ?", new String[]{String.valueOf(weather.getId())});
    }


    public void deleteWeather(Weather weather) {
        instance.getDatabase().delete(TABLE_WEATHER, COLUMN_WEATHER_ID + " = ?", new String[]{weather.getId()});
    }
}