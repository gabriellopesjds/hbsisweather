package br.com.gabriellopesjds.hbsisweather.mvp.util;

import android.view.View;

/**
 * Created by gabri on 18/01/2018.
 */

public interface RecyclerViewOnClickListenerHack {

    public void onClickListener(View view, int position);
    public void onLongClickListener(View view, int position);

}
