package br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherdetail;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.DetailWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.EventIntent;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;

public class CadastroWeatherDetailPresenter implements DetailWeatherMVP.Presenter {
    private DetailWeatherMVP.View view;
    private CadastroWeatherDetailModel model;
    private Weather weather;
    private String tipoIntent;
    private int position;

    public CadastroWeatherDetailPresenter(DetailWeatherMVP.View view) {
        this.view = view;
        this.model = new CadastroWeatherDetailModel(this);
        this.weather = new Weather();
    }

    @Override
    public Weather getWeather() {
        return this.weather;
    }

    @Override
    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    @Override
    public Context getContext() {
        return (Context) view;
    }

    @Override
    public void verificarTipoIntent(EventIntent eventIntent) {
        switch (eventIntent.getTipoIntent()) {
            case "CREATE":
                tipoIntent = "CREATE";
                break;
            case "UPDATE":
                this.weather = eventIntent.getWeather();
                this.position = eventIntent.getPosition();
                tipoIntent = "UPDATE";
                break;
            case "READ":
                this.weather = eventIntent.getWeather();
                tipoIntent = "READ";
                break;
        }
    }

    @Override
    public String getTipoIntent() {
        return this.tipoIntent;
    }

    @Override
    public int getPositionItemUpdate() {
        return this.position;
    }

    @Override
    public void getNetworkdataConsultaCidade(Weather weather) {
        model.getNetworkdataCidade(getContext(), weather);
    }

    @Override
    public void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response) {
        view.responseCallbackConsultaCidade(response);
    }

    @Override
    public void unregisterModel() {
        EventBus.getDefault().unregister(model);
    }
}