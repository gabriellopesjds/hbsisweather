package br.com.gabriellopesjds.hbsisweather.mvp.data;

import br.com.gabriellopesjds.hbsisweather.mvp.weather.DetailWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.ListWeatherMVP;

// ESTA CLASSE EXTENDE TODAS AS INTERFACES DE NETWORK
// DAS INTERFACES VIEW MODEL EVITANDO A IMPLEMENTAÇÃO MANUAL.
public interface NetworkHelper extends DetailWeatherMVP.Model.Network, ListWeatherMVP.Model.Network{
    void notificarConclusaoTarefa(Object object);
}
