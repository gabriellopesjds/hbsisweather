package br.com.gabriellopesjds.hbsisweather.mvp.base;

public abstract class BasePresenter<T> {
    protected T view;

    public BasePresenter(T view) {
        this.view = view;
    }

    public abstract T getView();
}
