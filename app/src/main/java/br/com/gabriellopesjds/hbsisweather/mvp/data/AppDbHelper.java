package br.com.gabriellopesjds.hbsisweather.mvp.data;

import android.content.Context;

import br.com.gabriellopesjds.hbsisweather.mvp.base.BaseHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.data.TarefaGravarCadastroWeather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public class AppDbHelper extends BaseHelper implements DbHelper {


    public AppDbHelper(AppDataManager appDataManager) {
        super(appDataManager);
    }

    @Override
    public void callServiceGravarCadastro(Weather weather, Context context, String tipoIntent) {
        TarefaGravarCadastroWeather tarefaGravarCliente = new TarefaGravarCadastroWeather(weather, context, tipoIntent,this);
        tarefaGravarCliente.execute();
    }

    @Override
    public void responseCallbackGravarCliente(ResponseCallbackGravarCliente response) {
        dataManager.responseCallbackGravarCliente(response);
    }

    @Override
    public void notificarConclusaoTarefa(Object object) {
        if (object instanceof ResponseCallbackGravarCliente)
            responseCallbackGravarCliente((ResponseCallbackGravarCliente) object);

    }
}