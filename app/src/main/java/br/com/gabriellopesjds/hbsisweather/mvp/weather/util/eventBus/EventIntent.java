package br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;

public class EventIntent {
    private Weather weather;
    private String tipoIntent;
    private int position;

    public EventIntent(Weather weather, String tipoIntent, int position) {
        this.weather = weather;
        this.tipoIntent = tipoIntent;
        this.position = position;
    }

    public Weather getWeather() {
        return weather;
    }

    public String getTipoIntent() {
        return tipoIntent;
    }

    public int getPosition() {
        return position;
    }
}
