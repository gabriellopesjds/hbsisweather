package br.com.gabriellopesjds.hbsisweather.mvp.data;


import br.com.gabriellopesjds.hbsisweather.mvp.weather.DetailWeatherMVP;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.ListWeatherMVP;

// ESTA CLASSE EXTENDE TODAS AS INTERFACES DE PREFERENCIA
// DAS INTERFACES VIEW MODEL EVITANDO A IMPLEMENTAÇÃO MANUAL.
public interface PreferencesHelper extends DetailWeatherMVP.Model.Preferences, ListWeatherMVP.Model.Preferences {
}
