package br.com.gabriellopesjds.hbsisweather.mvp.util;

public interface ConstantTables {
    static final String TABLE_WEATHER = "weather";
    static final String COLUMN_WEATHER_ID = "id_weather";
    static final String COLUMN_WEATHER_NOME_CIDADE = "nome_cidade";
    static final String COLUMN_WEATHER_NOME_PAIS = "nome_pais";
    static final String COLUMN_WEATHER_IMAGEM = "imagem";
    static final String COLUMN_WEATHER_TEMPERATURA_ATUAL = "temperatura";
    static final String COLUMN_WEATHER_HUMIDADE_ATUAL = "humidade";
    static final String COLUMN_WEATHER_PRESSAO_ATUAL = "pressao";
    static final String COLUMN_WEATHER_DATA_ATUALIZAZAO = "data";
}
