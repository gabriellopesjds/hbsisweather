package br.com.gabriellopesjds.hbsisweather.mvp.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.gabriellopesjds.hbsisweather.mvp.util.ConstantTables;

public class DbCore extends SQLiteOpenHelper implements ConstantTables {
    private static final String NOME_DO_BANCO = "db_hbsisweather";
    private static int VERSAO_DO_BANCO = 1;
    private Context context;

    private String CREATE_TABLE_WEATHER = "CREATE TABLE " + TABLE_WEATHER + "(" +
            COLUMN_WEATHER_ID + " VARCHAR(40) PRIMARY KEY," +
            COLUMN_WEATHER_NOME_CIDADE + " TEXT," +
            COLUMN_WEATHER_NOME_PAIS + " TEXT," +
            COLUMN_WEATHER_IMAGEM + " TEXT," +
            COLUMN_WEATHER_TEMPERATURA_ATUAL + " TEXT," +
            COLUMN_WEATHER_HUMIDADE_ATUAL + " TEXT," +
            COLUMN_WEATHER_PRESSAO_ATUAL + " TEXT," +
            COLUMN_WEATHER_DATA_ATUALIZAZAO + " TEXT);";

    public DbCore(Context context) {
        super(context, NOME_DO_BANCO, null, VERSAO_DO_BANCO);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_WEATHER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}