package br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;

/**
 * Created by gabri on 07/04/2018.
 */

public class ResponseCallbackCidade {
    private Weather weather;

    public ResponseCallbackCidade(Weather weather) {
        this.weather = weather;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }
}
