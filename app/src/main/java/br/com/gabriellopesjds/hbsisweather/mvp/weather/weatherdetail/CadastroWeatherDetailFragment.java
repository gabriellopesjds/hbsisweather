package br.com.gabriellopesjds.hbsisweather.mvp.weather.weatherdetail;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.com.gabriellopesjds.hbsisweather.R;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.DetailWeatherMVP;

public class CadastroWeatherDetailFragment extends Fragment {
    private DetailWeatherMVP.Presenter presenter;
    private TextView tvNomeCidade, tvDataAtualizacao, tvTemperaturaAtual, tvDescricao, tvHumidade, tvPressao;
    private ImageView ivIcone;
    private Weather weather;
    private RelativeLayout background;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_fragment_cadastro_weather_detail, null);
        initView(view);
        setValores(this.weather);
        return (view);
    }

    public void initView(View view) {
        background = (RelativeLayout) view.findViewById(R.id.rl_content_fragment);
        tvNomeCidade = (TextView) view.findViewById(R.id.tv_cidade);
        tvDataAtualizacao = (TextView) view.findViewById(R.id.tv_dataAtualizacao);
        tvDescricao = (TextView) view.findViewById(R.id.tv_descricao);
        tvTemperaturaAtual = (TextView) view.findViewById(R.id.tv_temperatura);
        tvHumidade = (TextView) view.findViewById(R.id.tv_humidade);
        tvPressao = (TextView) view.findViewById(R.id.tv_pressao);
        ivIcone = (ImageView) view.findViewById(R.id.icone);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object object) {
        if (object instanceof CadastroWeatherDetailPresenter) {
            this.presenter = (CadastroWeatherDetailPresenter) object;
            this.weather = presenter.getWeather();
        }

        if (object instanceof Weather) {
            this.weather = (Weather) object;
            setValores(this.weather);
        }
    }

    private void setValores(Weather weather) {
        presenter.setWeather(weather);
        tvNomeCidade.setText(weather.getNomeCidade() + " / " + weather.getNomePais());
        tvDataAtualizacao.setText(weather.getDataAtualizacao());
        tvDescricao.setText(weather.getDescricaoAtual());
        tvTemperaturaAtual.setText(" "+weather.getTemperaturaAtual() + " ℃");
        tvHumidade.setText("Umidade: " + weather.getHumidadeAtual());
        tvPressao.setText("Pressão: " + weather.getPressaoAtual());
        int drawableId = getContext().getResources().getIdentifier(weather.getImagem(), "drawable", getContext().getPackageName());
        ivIcone.setImageResource(drawableId);
        int drawableIdBackground = getResources().getIdentifier(weather.getBackground(), "drawable", getContext().getPackageName());
        background.setBackgroundResource(drawableIdBackground);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
        CadastroWeatherDetailPresenter stickyEvent = EventBus.getDefault().removeStickyEvent(CadastroWeatherDetailPresenter.class);
        if (stickyEvent != null) {
            EventBus.getDefault().removeStickyEvent(CadastroWeatherDetailPresenter.class);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.weather = ((CadastroWeatherDetailActivity) context).getWeather();
        EventBus.getDefault().register(this);
    }
}