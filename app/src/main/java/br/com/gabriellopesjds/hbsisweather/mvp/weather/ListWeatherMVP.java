package br.com.gabriellopesjds.hbsisweather.mvp.weather;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public interface ListWeatherMVP {
    interface Presenter {
        void callServiceGravarCadastro(Weather weather, String tipoIntent);
        void getNetworkdataCidade(String cidade);
        void unregisterModel();
        Context getContext();
        void responseCallbackCidade(ResponseCallbackCidade response);
        void responseCallbackGravarCliente(ResponseCallbackGravarCliente response);
        List<Weather> getWeatherList();
        List<Weather> getWeatherListAux();
        List<Weather> setWeatherList(List<Weather> list);
        List<Weather> setWeatherListAux(List<Weather> list);
    }

    interface View {
        void responseCallbackCidade(ResponseCallbackCidade response);
        void responseCallbackGravarCliente(ResponseCallbackGravarCliente response);
    }

    interface Model {
        void onMessageEvent(Object event);

        interface Db {
            void callServiceGravarCadastro(Weather weather, Context context, String tipoIntent);
            void responseCallbackGravarCliente(ResponseCallbackGravarCliente response);
            SQLiteDatabase getDatabase();
        }

        //Responsável por métodos de SharedPreferences
        interface Preferences {
        }

        // Responsavel por métodos de rede.
        interface Network {
            void getNetworkdataCidade(Context context, String cidade);
            void responseCallbackCidade(ResponseCallbackCidade response);
        }
    }
}