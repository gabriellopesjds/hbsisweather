package br.com.gabriellopesjds.hbsisweather.mvp.weather.data;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import br.com.gabriellopesjds.hbsisweather.mvp.data.DbHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.NetworkHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.db.dao.WeatherDAO;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public class TarefaGravarCadastroWeather extends AsyncTask<Void, Void, Weather> {
    private Context context;
    private Weather weather;
    private WeatherDAO dao;
    private ProgressDialog progress;
    private String tipoIntent;
    private DbHelper callback;

    public TarefaGravarCadastroWeather(Weather weather, Context context, String tipoIntent, DbHelper callback) {
        dao = new WeatherDAO(context);
        this.weather = weather;
        this.context = context;
        this.tipoIntent = tipoIntent;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        progress = new ProgressDialog(context);
        progress.setMessage("Gravando cadastro ...");
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(Weather weather) {
        progress.dismiss();
        callback.notificarConclusaoTarefa(new ResponseCallbackGravarCliente(weather, tipoIntent));
        super.onPostExecute(weather);
    }

    @Override
    protected Weather doInBackground(Void... params) {
        dao = new WeatherDAO(context);
        if (tipoIntent.equals("CREATE"))
            dao.addWeather(weather);
        else if (tipoIntent.equals("UPDATE"))
            dao.updateWeather(weather);
        return weather;
    }
}
