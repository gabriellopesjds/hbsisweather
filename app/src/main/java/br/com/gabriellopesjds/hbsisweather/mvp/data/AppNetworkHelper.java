package br.com.gabriellopesjds.hbsisweather.mvp.data;

import android.content.Context;

import br.com.gabriellopesjds.hbsisweather.mvp.base.BaseHelper;
import br.com.gabriellopesjds.hbsisweather.mvp.data.domain.Weather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.data.TarefaConsultarWeather;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackConsultaCidade;
import br.com.gabriellopesjds.hbsisweather.mvp.weather.util.eventBus.ResponseCallbackGravarCliente;

public class AppNetworkHelper extends BaseHelper implements NetworkHelper {
    public AppNetworkHelper(AppDataManager appDataManager) {
        super(appDataManager);
    }

    @Override
    public void getNetworkdataCidade(Context context, Weather weather) {
        TarefaConsultarWeather task = new TarefaConsultarWeather(context, this, weather);
        task.execute(weather.getNomeCidade());

    }

    @Override
    public void responseCallbackConsultaCidade(ResponseCallbackConsultaCidade response) {
        dataManager.responseCallbackConsultaCidade(response);
    }

    @Override
    public void getNetworkdataCidade(Context context, String cidade) {
        TarefaConsultarWeather task = new TarefaConsultarWeather(context, this);
        task.execute(cidade);
    }

    @Override
    public void responseCallbackCidade(ResponseCallbackCidade response) {
        dataManager.responseCallbackCidade(response);
    }

    @Override
    public void notificarConclusaoTarefa(Object object) {
        if (object instanceof ResponseCallbackConsultaCidade)
            responseCallbackConsultaCidade((ResponseCallbackConsultaCidade) object);
        if (object instanceof ResponseCallbackCidade)
            responseCallbackCidade((ResponseCallbackCidade) object);
    }
}
