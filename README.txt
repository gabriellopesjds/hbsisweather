Instru��es do projeto:
Para realizar o download do projeto, dever� acessar o repositorio do bibucket pelo link https://bitbucket.org/gabriellopesjds/hbsisweather e efetuar o download, caso trabalharem com git pode utilizar o comando "git clone https://bitbucket.org/gabriellopesjds/hbsisweather.git"

O Projeto foi desenvolvido na IDE Android Studio, portanto para fazer a importa��o via Android Studio, � s� acessar o menu FILE/OPEN e localizar o Folder do Repositorio.

Arquitetura:
O Padr�o de arquitetura utilizada no projeto foi a MVP ( Model, View, Presenter), uma representa��o que tem por objetivo fazer a separa��o dos conceitos trabalhando com uma arquitetura em camadas. Nessa representa��o o Model � respons�vel pelas conex�es com banco de dados, com Network, SharedPreferes, o Presenter � respons�vel por fazer valida��es e fazer a ponte de comunica��o entre a View e o Model, e p�r fim a View � respons�vel por atualizar a UI.

Um breve escopo foi representado pela imagem do Diagrama de Classes que est� na raiz do projeto. O Diagrama de classe mostra o escopo do sistema, por�m n�o representa todas as associa��es que existe dentro do projeto, � algo mais superficial para melhor visualiza��o e entendimento do projeto.

Nas libs utilizada, foi utilizada a Lib EventBus, que � uma lib para facilitar a comunica��o e transfer�ncias de objetos entre as classes do projeto.

Cada classe cuida da sua responsabilidade, no pacote MVP/data, temos DBHelper, NetworkHelper, PreferencesHelper, que s�o interfaces que herdam de todas as interfaces MVP dos modulos do sistema, como no presente sistema � basicamente 1 modulo, que � o modulo "weather", ent�o basicante s�o herdados os metodos das interfaces WeatherListMVP, WeatherDetailMVP.

O objetivo de utilizar as associa��es � centralizar toda as transa��es passando pelo DataManager que � o n�cleo principal da aplica��o, que por sua vez � uma interface que herdam DBHelper, NetworkHelper, PreferencesHelper, logo quando tivermos alguma implementa��o do DataManager que � o caso da classe AppDataManager, ele automaticamente herdar� todos os m�todos contidos nas interfaces, evitando assim as implementa��es manuais.

OBS: na Classe NetworkCidade, foi implementado a parte de deserializa��o do objeto para trabalhar com 5 dias, por�m foi observado que a API retorna de horas em horas. Para utilizar a API di�ria de at� 16 dias � necess�rio comprar uma licen�a, portanto foi feito apenas a previs�o atual.
